module antgamebattle {
    requires antgamesimulation;
    requires java.desktop;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;
    exports antgamebattle;
    exports antgamebattle.controller;
}