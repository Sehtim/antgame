package antgamebattle.controller;

import antgame.players.AntColony;
import antgame.simulation.Hive;
import antgame.simulation.Simulation;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class HiveDetailCardController implements Initializable {

    @FXML
    public GridPane rootPane;

    @FXML
    public Label hiveLocationLabel;
    @FXML
    public Label hiveFoodLabel;
    @FXML
    public Label hiveStrengthLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rootPane.setBorder(Border.stroke(Color.BLACK));
    }

    public void setHiveLocationLabel(Hive hive) {
        hiveLocationLabel.setText("Hive(" + hive.getLocation().x + "/" + hive.getLocation().y + ")");
    }

    public void updateLabels(Hive hive, Simulation authority) {
        AntColony controllingPlayer = hive.getControllingPlayer(authority);

        String foodString = "F: ";
        int food = (int) hive.getStoredFood(controllingPlayer);
        if (food < 10_000) {
            foodString += food;
        } else {
            foodString += food / 1000 + "K";
        }
        hiveFoodLabel.setText(foodString);

        String strengthString = "S: ";
        int strength = (int) hive.getTotalOffensiveStrength(controllingPlayer);
        if (strength < 10_000) {
            strengthString += strength;
        } else {
            strengthString += strength / 1000 + "K";
        }
        hiveStrengthLabel.setText(strengthString);
    }
}
