package antgamebattle.controller;

import antgame.players.AntColony;
import antgame.simulation.Hive;
import antgame.simulation.Simulation;
import antgamebattle.render.HelperFunctions;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class PlayerInfoPanelController implements Initializable {

    private AntColony player;

    private Simulation simulation;

    @FXML
    public Label playerNameLabel;
    @FXML
    public Label currentFoodLabel;
    @FXML
    public Label currentStrengthLabel;
    @FXML
    public Label numberOfHivesLabel;
    @FXML
    public Label antRatioLabel;

    @FXML
    public FlowPane detailFlowPane;
    private Map<Hive, Node> hiveToDetailCardMap = new HashMap<>();
    private Map<Node, HiveDetailCardController> detailCardToControllerMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setSimulation(Simulation simulation) {
        this.simulation = simulation;
    }

    public void setPlayerData(AntColony player) {
        this.player = player;

        playerNameLabel.setText(player.getColonyName());
        playerNameLabel.setTextFill(HelperFunctions.toFXMLColor(player.getColonyColor()));

        update();
    }

    public void update() {
        int totalFood = 0;
        int totalStrength = 0;
        int workerCount = 0;
        int scoutCount = 0;
        int warriorCount = 0;

        List<Hive> playerHives = simulation.getHiveListCopyOfPlayer(player);
        AntColony controllingPlayer;
        Set<Hive> survivedHives = new HashSet<>();
        for (Hive hive : playerHives) {
            controllingPlayer = hive.getControllingPlayer(simulation);
            totalFood += hive.getStoredFood(controllingPlayer);
            totalStrength += hive.getTotalOffensiveStrength(controllingPlayer);
            workerCount += hive.getCurrentWorkerCount(controllingPlayer);
            scoutCount += hive.getCurrentScoutCount(controllingPlayer);
            warriorCount += hive.getCurrentWarriorCount(controllingPlayer);

            survivedHives.add(hive);
            if (!hiveToDetailCardMap.containsKey(hive)) {
                Node hiveDetailCard = createHiveTile(hive);
                hiveToDetailCardMap.put(hive, hiveDetailCard);
                detailFlowPane.getChildren().add(hiveDetailCard);
            } else {
                detailCardToControllerMap.get(hiveToDetailCardMap.get(hive)).updateLabels(hive, simulation);
            }
        }

        Set<Hive> prevHives = new HashSet<>(hiveToDetailCardMap.keySet());
        prevHives.removeAll(survivedHives);
        for (Hive hive : prevHives) {
            Node hiveDetailCard = hiveToDetailCardMap.get(hive);
            detailFlowPane.getChildren().remove(hiveDetailCard);
            detailCardToControllerMap.remove(hiveDetailCard);
        }

        currentFoodLabel.setText(String.valueOf(totalFood));
        currentFoodLabel.setTextFill(Color.GREEN);

        currentStrengthLabel.setText(String.valueOf(totalStrength));
        currentStrengthLabel.setTextFill(Color.BLACK);

        numberOfHivesLabel.setText(String.valueOf(playerHives.size()));
        numberOfHivesLabel.setTextFill(Color.BROWN);

        antRatioLabel.setText(workerCount + "/" + scoutCount + "/" + warriorCount);
        antRatioLabel.setTextFill(Color.ORANGE);
    }

    private Node createHiveTile(Hive hive) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/HiveDetailCard.fxml"));
        try {
            Parent root = fxmlLoader.load(); // GridPane
            HiveDetailCardController controller = fxmlLoader.getController();
            controller.setHiveLocationLabel(hive);
            controller.updateLabels(hive, simulation);

            detailCardToControllerMap.put(root, controller);

            return root;
        } catch (IOException e) {
            System.err.println("Cannot load Player Info Panel!");
            e.printStackTrace();

            return null;
        }
    }
}
