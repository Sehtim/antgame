package antgamebattle.controller;

import antgame.WorldModel;
import antgame.players.AntColony;
import antgame.poi.FruitTree;
import antgame.simulation.Hive;
import antgame.simulation.PointOfInterest;
import antgame.simulation.Simulation;
import antgamebattle.render.HelperFunctions;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import antgame.data.Point;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class BattleGUIController implements Initializable {

    private static final double FPS_CAP = 30.0;

    @FXML
    public Pane canvasPane;
    public Canvas battleMap;

    @FXML
    public VBox playerInfoPane;
    List<PlayerInfoPanelController> playerInfoControllerList;

    @FXML
    public Slider simulationSpeedSlider;
    volatile boolean paused = false;

    private GraphicsContext gc;
    Timeline rendering;
    private volatile boolean shouldUpdateInfo;
    private volatile boolean shouldRedraw;

    private final WorldModel worldModel;
    private final Simulation simulation;

    private double mouseDragPointX;
    private double mouseDragPointY;
    private volatile double drawingOffsetX;
    private volatile double drawingOffsetY;

    public BattleGUIController(WorldModel worldModel, Simulation simulation) {
        this.worldModel = worldModel;
        this.simulation = simulation;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        battleMap = new Canvas();
        canvasPane.getChildren().add(battleMap);
        // Bind canvas size to pane size to make it resizable
        // Credits to: https://stackoverflow.com/questions/24533556/how-to-make-canvas-resizable-in-javafx
        // Credits to: https://dlemmermann.wordpress.com/2014/04/10/javafx-tip-1-resizable-canvas/
        battleMap.widthProperty().bind(canvasPane.widthProperty());
        battleMap.heightProperty().bind(canvasPane.heightProperty());

        gc = battleMap.getGraphicsContext2D();
        battleMap.setOnMousePressed(event -> {
            mouseDragPointX = event.getX();
            mouseDragPointY = event.getY();
        });
        battleMap.setOnMouseDragged(event -> {
            //noinspection NonAtomicOperationOnVolatileField
            drawingOffsetX += event.getX() - mouseDragPointX; // Volatile field is only written here
            //noinspection NonAtomicOperationOnVolatileField
            drawingOffsetY += event.getY() - mouseDragPointY; // Volatile field is only written here
            mouseDragPointX = event.getX();
            mouseDragPointY = event.getY();
            shouldRedraw = true;
        });

//        battleMap.setOnMouseClicked(event -> {
//            System.out.println("Test Clicked!");
//        });

        // Using a timeline to render the simulation
        // Credits to: https://stackoverflow.com/questions/36048234/javafx-canvas-stops-displaying-after-a-few-seconds-im-trying-million-ovals
        rendering = new Timeline(
                new KeyFrame(
                        Duration.seconds(0),
                        event -> {
                            drawModel();
                            updatePlayerInfo();
                        }
                ),
                new KeyFrame(Duration.millis(1.0 / FPS_CAP * 1000.0))
        );
        rendering.setCycleCount(Timeline.INDEFINITE);
        rendering.play();
    }

    public void stop() {
        if (rendering != null) {
            rendering.stop();
        }
        synchronized (this) {
            paused = false;
            this.notifyAll();
        }
    }

    /**
     * Called when button "Pause" is pressed.
     */
    public void pauseGame() {
        paused = true;
    }

    /**
     * Called when button "Resume" is pressed.
     */
    public synchronized void resumeGame() {
        paused = false;
        this.notifyAll();
    }

    public boolean isPaused() {
        return paused;
    }

    public int getSimulationSpeed() {
        return (int) (simulationSpeedSlider.getMax() - simulationSpeedSlider.getValue());
    }

    public synchronized void stepOneDay() {
        this.notifyAll();
    }

    public void worldModelUpdated() {
        shouldRedraw = true;
        shouldUpdateInfo = true;
    }

    private void drawModel() {
        if (!shouldRedraw) {
            return;
        }
        synchronized (worldModel) {
            gc.clearRect(0, 0, battleMap.getWidth(), battleMap.getHeight());
            worldModel.getScoutAreas().forEach(scoutArea -> {
                if (scoutArea.getAreaSize() > 500) {
                    gc.setStroke(HelperFunctions.toFXMLColor(scoutArea.getControllingPlayer(simulation).getColonyColor()));
                    Point center = scoutArea.getCenter(simulation);
                    if (center != null) {
                        int radius = scoutArea.getRadius(simulation);
                        gc.strokeOval(center.x + drawingOffsetX - radius, center.y + drawingOffsetY - radius, radius * 2, radius * 2);
                    }
                }
            });

            worldModel.getGlobalAntDistribution().forEach((poi, localAntGroups) -> {
                localAntGroups.forEach(group -> {
                    if (group.getAntCount() > 10) {
                        gc.setStroke(HelperFunctions.toFXMLColor(group.getControllingPlayer(simulation).getColonyColor()));
                        gc.strokeLine(group.getControllingHive(simulation).getLocation().x + drawingOffsetX, group.getControllingHive(simulation).getLocation().y + drawingOffsetY, poi.getLocation().x + drawingOffsetX, poi.getLocation().y + drawingOffsetY);
                    }
                });
            });

            // Sort the POIs by Y, to draw from back to front
            List<PointOfInterest> pointOfInterestList = new ArrayList<>(worldModel.getPointsOfInterest());
            pointOfInterestList.sort(Comparator.comparingInt(poi -> poi.getLocation().y + ((poi instanceof Hive) ? 100 : 0)));
            for (PointOfInterest poi : pointOfInterestList) {
                switch (poi) {
                    case Hive hive -> drawHive(hive);
                    case FruitTree tree -> drawFruitTree(tree);
                    default -> throw new IllegalStateException("Unexpected value: " + poi);
                }
            }
        }
        shouldRedraw = false;
    }

    private void drawHive(Hive hive) {
        int radius = (int) Math.log(hive.getCurrentAntCount(hive.getControllingPlayer(simulation)) * 1000.0);
        if (radius == 0) {
            radius = 1;
        }
        gc.setStroke(HelperFunctions.toFXMLColor(hive.getControllingPlayer(simulation).getColonyColor()));
        gc.strokeOval(hive.getLocation().x + drawingOffsetX - radius, hive.getLocation().y + drawingOffsetY - radius, radius * 2, radius * 2);

        // Credits to Marcel Valdeig
        int availableFood = (int) hive.getStoredFood(hive.getControllingPlayer(simulation));
        String availableFoodString;
        int fontSize;
        double drawX = hive.getLocation().x + drawingOffsetX;
        double drawY = hive.getLocation().y + drawingOffsetY;
        if (availableFood < 10_000) {
            drawY += radius + 11;
            fontSize = 12;
            availableFoodString = String.valueOf(availableFood);
        } else {
            drawY += radius + 13;
            fontSize = 14;
            availableFoodString = availableFood / 1_000 + "K";
        }

        gc.setStroke(Color.BLACK);
        gc.setFill(Color.GREEN);
        gc.setFont(Font.font("System", FontWeight.BOLD, fontSize));
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText(availableFoodString, drawX, drawY);

        drawY += fontSize;
        String combatStrength;
        int strength = (int) hive.getTotalOffensiveStrength(hive.getControllingPlayer(simulation));
        if (strength < 10_000) {
            fontSize = 12;
            combatStrength = String.valueOf(strength);
        } else {
            drawY += 2;
            fontSize = 14;
            combatStrength = strength / 1000 + "K";
        }
        gc.setFill(Color.BLACK);
        gc.setFont(Font.font("System", FontWeight.BOLD, fontSize));
        gc.fillText(combatStrength, drawX, drawY);
    }

    private void drawFruitTree(FruitTree tree) {
        Point loc = tree.getLocation();
        gc.setStroke(Color.BLACK);
        gc.setFill(Color.GREEN);
        int availableFood = (int) tree.getFoodAvailable(simulation);
        int drawSize = availableFood / 1000;
        if (drawSize < 4) {
            drawSize = 4;
        }
        double drawX = loc.x + drawingOffsetX;
        double drawY = loc.y + drawingOffsetY;

        gc.fillPolygon(new double[]{drawX, drawX - drawSize, drawX + drawSize}, new double[]{drawY - drawSize * 1.5, drawY + drawSize, drawY + drawSize}, 3);

        // Credits to Marcel Valdeig
        String availableFoodString;
        int fontSize;
        if (availableFood < 10_000) {
            drawY += drawSize + 11;
            fontSize = 12;
            availableFoodString = String.valueOf(availableFood);
        } else {
            drawY += drawSize + 13;
            fontSize = 14;
            availableFoodString = availableFood / 1_000 + "K";
        }

        gc.setFont(Font.font("System", FontWeight.BOLD, fontSize));
        gc.setTextAlign(TextAlignment.CENTER);
        gc.fillText(availableFoodString, drawX, drawY);
    }

    public void setPlayerHiveMap(Map<AntColony, List<Hive>> players) {
        // Info: ScrollPane works with dynamical content, if "playerInfoPane" got preferred height
        Map<AntColony, List<Hive>> playersCopy = new HashMap<>(players); // Avoid ConcurrentModification
        Platform.runLater(() -> {
            playerInfoPane.getChildren().clear();
            playerInfoControllerList = new ArrayList<>(playersCopy.size());

            for (AntColony player : playersCopy.keySet()) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/PlayerInfoPanel.fxml"));
                try {
                    Parent root = fxmlLoader.load(); // BorderPane
                    PlayerInfoPanelController controller = fxmlLoader.getController();
                    controller.setSimulation(simulation);
                    controller.setPlayerData(player);
                    playerInfoControllerList.add(controller);

                    playerInfoPane.getChildren().add(root);
                } catch (IOException e) {
                    System.err.println("Cannot load Player Info Panel!");
                    e.printStackTrace();
                }
            }
        });
    }

    private void updatePlayerInfo() {
        if (!shouldUpdateInfo) {
            return;
        }
        if (playerInfoControllerList != null) {
            for (PlayerInfoPanelController controller : playerInfoControllerList) {
                controller.update();
            }
        }
        shouldUpdateInfo = false;
    }
}
