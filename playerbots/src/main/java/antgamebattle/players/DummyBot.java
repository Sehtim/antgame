package antgamebattle.players;

import antgame.data.PlayerHiveReport;
import antgame.players.AntColony;
import antgame.simulation.AntGroup;
import antgame.simulation.Hive;
import antgame.simulation.PointOfInterest;
import antgame.simulation.ScoutArea;
import antgame.data.Color;
import antgame.data.Point;

import java.util.Map;
import java.util.function.Function;

public class DummyBot implements AntColony {

    private Color assignedColor = new Color(1.0, 0.0, 0.0);

    @Override
    public String getColonyName() {
        return "The Dummy Ants";
    }

    @Override
    public Color getColonyColor() {
        return assignedColor;
    }

    @Override
    public void assignNewColor(Color color) {
        assignedColor = color;
    }

    @Override
    public void nextDay() {

    }

    @Override
    public void dailyReport(Hive myHive, PlayerHiveReport report) {

    }

    @Override
    public AntGroup planBreeding(Hive myHive) {
        return null;
    }

    @Override
    public ScoutArea scoutArea(Hive myHive) {
        return null;
    }

    @Override
    public AntGroup sendNewHiveGroup(Hive myHive, Point plannedLocation) {
        return null;
    }

    @Override
    public Point planNewHiveSendQueenTo(Hive myHive, Function<Point, Boolean> testValidPoint) {
        return null;
    }

    @Override
    public Map<PointOfInterest, AntGroup> distributeAnts(Hive myHive) {
        return null;
    }

    @Override
    public void hiveDestroyed(Hive myHive) {

    }
}
