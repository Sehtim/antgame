package antgamebattle;

import antgame.WorldModel;
import antgame.players.AntColony;
import antgame.simulation.Simulation;
import antgamebattle.controller.BattleGUIController;
import antgamebattle.players.*;
import javafx.application.Application;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BattleGUI extends Application {

    private static final String SEED = "Kolosseum"; // Kolosseum // TheProvingGround
    private static final int WORLD_SIZE_X = 2000;
    private static final int WORLD_SIZE_Y = 2000;
    private static final int DO_X_RUNS = 10;
    private static final int DRAW_AFTER_DAYS = 8000;

    private BattleGUIController mainController;
    private volatile boolean running = false;

    private final WorldModel worldModel;
    private final Simulation simulation;

    public BattleGUI() {
        worldModel = new WorldModel();
        simulation = new Simulation(worldModel);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/BattleGUI.fxml"));
            fxmlLoader.setControllerFactory((clazz) -> new BattleGUIController(worldModel, simulation));
            Parent root = fxmlLoader.load(); // BorderPane
            Scene scene = new Scene(root, 1200, 741);
            scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/fxml/application.css")).toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setTitle("Antgame Battle GUI");
            // copied from https://www.debugcn.com/en/article/62669270.html
            Window.getWindows().addListener((ListChangeListener<Window>) c -> {
                while (c.next()) {
                    for (Window window : c.getAddedSubList()) {
                        if (window instanceof Stage) {
//                            ((Stage) window).getIcons().add(new Image(getClass().getResourceAsStream("/Logo.png")));
                        }
                    }
                }
            });

            mainController = fxmlLoader.getController();

            primaryStage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closeWindowEvent);

            primaryStage.show();

            running = true;
            new Thread(this::runSimulation).start();
        } catch (IOException e) {
            System.err.println("Error while starting the GUI.");
            e.printStackTrace();
        }
    }

    private void closeWindowEvent(WindowEvent event) {
        running = false;
        mainController.stop();
    }

    private void runSimulation() {
        System.out.println("Starting Simulation...");

        Map<String, Integer> winCounterMap = new HashMap<>();
        for (int i = 0; i < DO_X_RUNS && running; i++) {
            simulation.resetSimulation();
            System.out.println("Starting run " + (i + 1) + "/" + DO_X_RUNS + ".");
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.addPlayer(new DummyBot());
            simulation.generateNewWorld(SEED.hashCode(), WORLD_SIZE_X, WORLD_SIZE_Y); // TODO possibility to use random seed every iteration
            mainController.setPlayerHiveMap(simulation.getPlayers());
            while (running) {
                simulation.simulateDay();
                mainController.worldModelUpdated(); // TODO change design to Listener
                AntColony winner = simulation.getWinner();
                if (winner != null) {
                    System.out.println(winner.getColonyName() + " has won the game after " + simulation.getCurrentDay() + " days!");
                    winCounterMap.merge(winner.getColonyName(), 1, Integer::sum);
                    break;
                }
                if (simulation.getCurrentDay() > DRAW_AFTER_DAYS) {
                    System.out.println("Draw after " + DRAW_AFTER_DAYS + " days!");
                    for (AntColony survivor : simulation.getPlayers().keySet()) {
                        winCounterMap.merge(survivor.getColonyName(), 1, Integer::sum);
                    }

                    break;
                }
                //noinspection SynchronizeOnNonFinalField
                synchronized (mainController) { // Can not make it final, because it is assigned in start()
                    try {
                        if (mainController.isPaused()) {
                            mainController.wait();
                        } else {
                            mainController.wait(mainController.getSimulationSpeed());
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
            System.out.println("Result:");
            for (Map.Entry<String, Integer> entry : winCounterMap.entrySet()) {
                System.out.println("Player " + entry.getKey() + ": " + entry.getValue() + " wins.");
            }
        }
    }
}
