package antgamebattle.render;

import antgame.data.Color;

public class HelperFunctions {
    public static javafx.scene.paint.Color toFXMLColor(Color color) {
        return new javafx.scene.paint.Color(color.getRed(), color.getGreen(), color.getBlue(), color.getOpacity());
    }

    public static Color toAntColor(javafx.scene.paint.Color color) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getOpacity());
    }
}
