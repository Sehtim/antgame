package antgame.players;

import antgame.GameConstants;
import antgame.data.Color;
import antgame.data.PlayerHiveReport;
import antgame.data.Point;
import antgame.simulation.AntGroup;
import antgame.simulation.Hive;
import antgame.simulation.PointOfInterest;
import antgame.simulation.ScoutArea;

import java.util.Map;
import java.util.function.Function;

public interface AntColony {
    /**
     * @return The name of this colony for display purposes.
     */
    String getColonyName();

    /**
     * @return The color of this colony for display purposes.
     */
    Color getColonyColor();

    /**
     * This method is called from the simulation in the init process to make the colors of the ant colonies distinguishable.
     * The simulation expects, that the set color is returned by consecutive calls of {@link #getColonyColor()}.
     *
     * @param color New color of the ant colony.
     */
    void assignNewColor(Color color);

    /**
     * This method is called once per day at the beginning of the day. Note that it is not called per hive but per ant colony.
     */
    void nextDay();

    /**
     * This method is called once per day per hive and provides information about what happened the last day.
     *
     * @param myHive The hive, which collected the information.
     * @param report The report containing the collected information.
     * @see PlayerHiveReport
     */
    void dailyReport(Hive myHive, PlayerHiveReport report);

    /**
     * Plan how many ants you want to breed at the given hive. Food will be subtracted this day. You will get the bred ants the next day.
     * <br>
     * - If there is not enough food available, nothing will happen. <br>
     * - If the hive cannot hold the additional amount of ants at the day when this method is called, nothing will happen.
     *
     * @param myHive The hive, which has to decide, if it wants to breed. Food will get subtracted from this hive.
     * @return An {@link AntGroup}, which contains the amount of workers, scouts and warriors you want to breed. Null can be used to breed nothing.
     * @see Hive#getMaxAntCount
     * @see GameConstants#FOOD_COST_WORKER
     * @see GameConstants#FOOD_COST_SCOUT
     * @see GameConstants#FOOD_COST_WARRIOR
     */
    AntGroup planBreeding(Hive myHive);

    /**
     * Plan where the scouts should look for new {@link PointOfInterest}s.
     * The found POIs will be put into the {@link PlayerHiveReport}, which is passed in the next call of {@link #dailyReport(Hive, PlayerHiveReport)}.
     * <br>
     * - If the returned {@link ScoutArea} has set the controllingHive wrong, nothing will happen. <br>
     * - If the hive sends more scouts than the hive has available, nothing will happen.
     *
     * @param myHive The hive, which has to decide where to scout and how many scouts to use.
     * @return A {@link ScoutArea}, which contains the center and radius of the area and the number of scouts to use. Null can be used to skip scouting for the day.
     * @see GameConstants#SCOUT_AREA_CHANCE_MODIFIER
     */
    ScoutArea scoutArea(Hive myHive);

    AntGroup sendNewHiveGroup(Hive myHive, Point plannedLocation);

    Point planNewHiveSendQueenTo(Hive myHive, Function<Point, Boolean> testValidPoint);

    Map<PointOfInterest, AntGroup> distributeAnts(Hive myHive);

    void hiveDestroyed(Hive myHive);

    static Point getPointOnCircle(Point center, float distance, float degrees) {
        return new Point((int) (center.x + distance * Math.cos(Math.toRadians(degrees))), (int) (center.y - distance * Math.sin(Math.toRadians(degrees))));
    }
}
