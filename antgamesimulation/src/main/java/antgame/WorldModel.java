package antgame;

import antgame.simulation.AntGroup;
import antgame.simulation.ScoutArea;
import antgame.simulation.PointOfInterest;
import antgame.data.Point;

import java.util.*;
import java.util.List;

public class WorldModel {
    // Reminder: When adding new member variables, check if it has to be reset in resetData()
    private final Set<PointOfInterest> pointsOfInterest = new HashSet<>();
    private final Set<ScoutArea> scoutAreas = new HashSet<>();
    private final Map<PointOfInterest, List<AntGroup>> globalAntDistribution = new HashMap<>();

    public void resetData() {
        pointsOfInterest.clear();
        scoutAreas.clear();
        globalAntDistribution.clear();
    }

    public boolean isPointNearPOI(Class<? extends PointOfInterest> type, Point point, float distance) {
        for (PointOfInterest poi : pointsOfInterest) {
            if (type.isInstance(poi) && poi.getDistanceTo(point) <= distance) {
                return true;
            }
        }
        return false;
    }

    public Set<PointOfInterest> getPointsOfInterest() {
        return pointsOfInterest;
    }

    public void addPointOfInterest(PointOfInterest newPOI) {
        pointsOfInterest.add(newPOI);
    }

    public void removePointOfInterest(PointOfInterest deletePOI) {
        pointsOfInterest.remove(deletePOI);
    }

    public Set<ScoutArea> getScoutAreas() {
        return scoutAreas;
    }

    public void addScoutArea(ScoutArea area) {
        scoutAreas.add(area);
    }

    public void clearScoutAreas() {
        scoutAreas.clear();
    }

    public Map<PointOfInterest, List<AntGroup>> getGlobalAntDistribution() {
        return globalAntDistribution;
    }
}
