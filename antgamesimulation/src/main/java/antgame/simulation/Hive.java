package antgame.simulation;

import antgame.GameConstants;
import antgame.players.AntColony;
import antgame.data.Point;

import static antgame.GameConstants.*;

public class Hive extends PointOfInterest {
    private final AntColony controllingPlayer;
    private int currentWorkerCount;
    private int currentScoutCount;
    private int currentWarriorCount;
    private int maxAntCount;
    private float storedFood;
    private float maxStoredFood;
    private boolean destroyed = false;

    public Hive(Simulation authority, Point location, AntColony controllingPlayer, int startWorker, int startScout, int startWarrior, int startFood, int startAntCapacity, int startFoodCapacity) {
        super(authority, location);
        this.controllingPlayer = controllingPlayer;
        this.currentWorkerCount = startWorker;
        this.currentScoutCount = startScout;
        this.currentWarriorCount = startWarrior;
        this.maxAntCount = startAntCapacity;
        this.storedFood = startFood;
        this.maxStoredFood = startFoodCapacity;
    }

    @Override
    void simulateDay(int currentDayOfYear) {
        eatFood();
        foodDecay();
        naturalDeath();
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    void destroy() {
        destroyed = true;
    }

    private void eatFood() {
        float foodConsumption = currentWorkerCount * FOOD_DRAIN_WORKER
                + currentScoutCount * FOOD_DRAIN_SCOUT
                + currentWarriorCount * FOOD_DRAIN_WARRIOR
                + FOOD_DRAIN_QUEEN;
        if (storedFood >= foodConsumption) {
            storedFood -= foodConsumption;
        } else {
            // Hunger
            float survivingRate = 1 - FOOD_HUNGER_DEATH_RATE * (1 - storedFood / foodConsumption);
            currentWorkerCount *= survivingRate;
            currentScoutCount *= survivingRate;
            currentWarriorCount *= survivingRate;
            storedFood = 0.0f;
        }
    }

    private void foodDecay() {
        storedFood *= 1 - FOOD_DECAY_RATE;
    }

    private void naturalDeath() {
        currentWorkerCount *= 1 - HIVE_NATURAL_DEATH_RATE;
        currentScoutCount *= 1 - HIVE_NATURAL_DEATH_RATE;
        currentWarriorCount *= 1 - HIVE_NATURAL_DEATH_RATE;
    }

    void collectFood(float amount) {
        storedFood += amount;
        if (storedFood > maxStoredFood) {
            storedFood = maxStoredFood;
        }
        // System.out.println("We gathered " + amount + " food. Hive got " + storedFood + " now.");
    }

    void useFood(float amount) {
        if (amount > storedFood)
            throw new IllegalArgumentException("Can not use more food than stored.");
        storedFood -= amount;
    }

    void buildBiggerHive(int workers) {
        maxStoredFood += HIVE_GROW_FOOD_CAPACITY * HIVE_GROW_PER_WORKER_FACTOR * workers;
        maxAntCount += HIVE_GROW_ANT_CAPACITY * HIVE_GROW_PER_WORKER_FACTOR * workers;
    }

    void addAntGroup(AntGroup newAnts) {
        currentWorkerCount += newAnts.getWorkerCount();
        currentScoutCount += newAnts.getScoutCount();
        currentWarriorCount += newAnts.getWarriorCount();
        // System.out.println("Hive got now " + (currentWorkerCount + currentScoutCount + currentWarriorCount) + " ants.");
    }

    void removeAntGroup(AntGroup loseAnts) {
        currentWorkerCount -= loseAnts.getWorkerCount();
        currentScoutCount -= loseAnts.getScoutCount();
        currentWarriorCount -= loseAnts.getWarriorCount();
    }

    boolean isEmpty() {
        return currentWorkerCount == 0 && currentScoutCount == 0 && currentWarriorCount == 0;
    }

    public AntColony getControllingPlayer(Simulation authority) {
        if (this.authority != authority) {
            throw new IllegalCallerException("This information is not available for bots.");
        }
        return controllingPlayer;
    }

    public boolean isEnemyOf(AntColony player) {
        return controllingPlayer != player;
    }

    public int getCurrentAntCount(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1;
        }
        return currentWorkerCount + currentScoutCount + currentWarriorCount;
    }

    public int getCurrentWorkerCount(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1;
        }
        return currentWorkerCount;
    }

    public int getCurrentScoutCount(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1;
        }
        return currentScoutCount;
    }

    public int getCurrentWarriorCount(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1;
        }
        return currentWarriorCount;
    }

    public int getMaxAntCount(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1;
        }
        return maxAntCount;
    }

    public float getStoredFood(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1.0f;
        }
        return storedFood;
    }

    public float getMaxStoredFood(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1.0f;
        }
        return maxStoredFood;
    }

    public float getTotalOffensiveStrength(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1.0f;
        }
        return currentWorkerCount * GameConstants.COMBAT_STRENGTH_WORKER
                + currentScoutCount * GameConstants.COMBAT_STRENGTH_SCOUT
                + currentWarriorCount * GameConstants.COMBAT_STRENGTH_WARRIOR;
    }

    public float getTotalDefensiveStrength(AntColony authentication) {
        if (authentication != controllingPlayer) {
            return -1.0f;
        }
        return (currentWorkerCount * GameConstants.COMBAT_STRENGTH_WORKER
                + currentScoutCount * GameConstants.COMBAT_STRENGTH_SCOUT
                + currentWarriorCount * GameConstants.COMBAT_STRENGTH_WARRIOR
                + COMBAT_STRENGTH_QUEEN) * (1.0f + COMBAT_HIVE_DEFENSE_BONUS);
    }
}
