package antgame.simulation;

import antgame.players.AntColony;

import static antgame.GameConstants.*;

public class AntGroup {
    private final Hive controllingHive;
    private int workerCount;
    private int scoutCount;
    private int warriorCount;

    public AntGroup(Hive controllingHive, int workerCount, int scoutCount, int warriorCount) {
        this.controllingHive = controllingHive;
        if (workerCount < 0) {
            System.out.println("Antgroups can not have negative numbers of ants!");
        } else {
            this.workerCount = workerCount;
        }
        if (scoutCount < 0) {
            System.out.println("Antgroups can not have negative numbers of ants!");
        } else {
            this.scoutCount = scoutCount;
        }
        if (warriorCount < 0) {
            System.out.println("Antgroups can not have negative numbers of ants!");
        } else {
            this.warriorCount = warriorCount;
        }
    }

    AntGroup getPercentageOfGroup(float percent) {
        return new AntGroup(controllingHive, (int) (workerCount * percent), (int) (scoutCount * percent), (int) (warriorCount * percent));
    }

    /**
     * This AntGroup loses a percentage of all its units. As a result the method gives a new AntGroup which consists of the number of units lost.
     *
     * @param percent Number from 0 to 1 (inclusive). 0 means no change to the object and 1 means this AntGroup will be empty of units.
     * @return A new AntGroup with the number of units lost.
     */
    AntGroup losePercentOfGroup(float percent) {
        if (percent > 1.0f)
            throw new IllegalArgumentException("An AntGroup can not lose more than 100% of its units.");
        AntGroup losses = getPercentageOfGroup(percent);
        workerCount -= losses.workerCount;
        scoutCount -= losses.scoutCount;
        warriorCount -= losses.warriorCount;
        // System.out.println("Losing " + (workersLost + scoutsLost + warriorsLost) + " ants of group.");
        return losses;
    }

    AntGroup merge(AntGroup otherGroup) {
        if (controllingHive != otherGroup.controllingHive)
            throw new IllegalArgumentException("Merging AntGroups are not controlled by the same player!");
        return new AntGroup(controllingHive,
                workerCount + otherGroup.workerCount,
                scoutCount + otherGroup.scoutCount,
                warriorCount + otherGroup.warriorCount);
    }

    Hive getControllingHive() {
        return controllingHive;
    }

    public Hive getControllingHive(Simulation authority) {
        if (controllingHive.authority == authority) {
            return controllingHive;
        }
        return null;
    }

    public AntColony getControllingPlayer(Simulation authority) {
        return controllingHive.getControllingPlayer(authority);
    }

    public int getWorkerCount() {
        return workerCount;
    }

    public int getScoutCount() {
        return scoutCount;
    }

    public int getWarriorCount() {
        return warriorCount;
    }

    public int getAntCount() {
        return workerCount + scoutCount + warriorCount;
    }

    public float getCombatStrength(PointOfInterest travelDestination) {
        return (float) ((workerCount * COMBAT_STRENGTH_WORKER + scoutCount * COMBAT_STRENGTH_SCOUT + warriorCount * COMBAT_STRENGTH_WARRIOR)
                * Math.pow(1 - COMBAT_TRAVEL_EXHAUSTION_MALUS, getControllingHive().getDistanceTo(travelDestination.getLocation()) / 100.0f));
    }
}
