package antgame.simulation;

import antgame.GameConstants;
import antgame.data.Gaussian;
import antgame.data.Point;

public abstract class FoodSource extends PointOfInterest {

    private float foodAvailable;
    private final int foodProductionPeakPerDay;
    private final int foodProductionPeakDayOfYear;
    private final int dangerLevel;
    private final Gaussian foodProductionDistribution;

    public FoodSource(Simulation authority, Point location, float foodAvailable, int foodProductionPeakPerDay, int foodProductionPeakDayOfYear, int dangerLevel, int foodStdDeviation) {
        super(authority, location);
        this.foodAvailable = foodAvailable;
        this.foodProductionPeakPerDay = foodProductionPeakPerDay;
        this.foodProductionPeakDayOfYear = foodProductionPeakDayOfYear;
        this.dangerLevel = dangerLevel;
        this.foodProductionDistribution = new Gaussian(foodStdDeviation);
    }

    public float getFoodAvailable(Simulation authority) {
        if (this.authority != authority) {
            throw new IllegalCallerException("This information is not available for bots.");
        }
        return foodAvailable;
    }

    int getDangerLevel() {
        return dangerLevel;
    }

    void foodGathered(float amount) {
        foodAvailable -= amount;
    }

    @Override
    void simulateDay(int currentDayOfYear) {
        foodDecay();
        produceFood(currentDayOfYear);
    }

    private void foodDecay() {
        foodAvailable *= 1 - GameConstants.FOOD_DECAY_RATE;
    }

    float getCurrentFoodProduction(int currentDayOfYear) {
        int daysFromPeak = currentDayOfYear - foodProductionPeakDayOfYear;
        if (daysFromPeak < -182)
            daysFromPeak += 365;
        else if (daysFromPeak > 182)
            daysFromPeak -= 365;

        return (float) (foodProductionPeakPerDay * foodProductionDistribution.getY(daysFromPeak));
    }

    private void produceFood(int currentDayOfYear) {
        foodAvailable += getCurrentFoodProduction(currentDayOfYear);
    }
}
