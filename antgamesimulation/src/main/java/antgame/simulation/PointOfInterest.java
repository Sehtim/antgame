package antgame.simulation;

import antgame.data.Point;

public abstract class PointOfInterest {
    final Simulation authority;
    private final Point location;

    public PointOfInterest(Simulation authority, Point location) {
        this.authority = authority;
        this.location = location;
    }

    public Point getLocation() {
        return this.location;
    }

    public double getDistanceTo(Point point) {
        return Math.sqrt(Math.pow(point.x - location.x, 2) + Math.pow(point.y - location.y, 2));
    }

    void simulateDay(int currentDayOfYear) {
        throw new UnsupportedOperationException("PointOfInterest can not simulate.");
    }
}
