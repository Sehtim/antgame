package antgame.simulation;

import antgame.WorldModel;
import antgame.data.Color;
import antgame.data.PlayerHiveReport;
import antgame.data.PoiIntel;
import antgame.players.AntColony;
import antgame.poi.FruitTree;
import antgame.data.Point;

import java.util.List;
import java.util.*;

import static antgame.GameConstants.*;

public class Simulation {
    // Reminder: When adding new member variables, check if it has to be reset in resetSimulation()
    private final WorldModel worldModel;
    private final Map<AntColony, List<Hive>> players = new HashMap<>();
    private final Map<Hive, PlayerHiveReport> playerHiveReports = new HashMap<>();
    private final Map<Hive, Integer> playerExplorationScoutCount = new HashMap<>();
    private final Map<Hive, AntGroup> playerBreedingAnts = new HashMap<>();
    private final Map<Hive, PlannedHive> playerPlannedHives = new HashMap<>();
    private final List<AntColony> playersDiedThisDay = new ArrayList<>();
    private Random randomGenerator;
    private int maxWidth, maxHeight;
    private int currentDay, dayOfYear;

    private record PlannedHive(Point location, int turns) {
        public double getDistanceTo(Point point) {
            return Math.sqrt(Math.pow(point.x - location.x, 2) + Math.pow(point.y - location.y, 2));
        }
    }

    public Simulation(WorldModel worldModel) {
        this.worldModel = worldModel;
    }

    public void resetSimulation() {
        synchronized (worldModel) {
            worldModel.resetData();
        }
        synchronized (players) {
            players.clear();
        }
        playerHiveReports.clear();
        playerExplorationScoutCount.clear();
        playerBreedingAnts.clear();
        playerPlannedHives.clear();
        playersDiedThisDay.clear();
        currentDay = 0;
        dayOfYear = 0;
    }

    public void addPlayer(AntColony player) {
        synchronized (players) {
            players.put(player, new ArrayList<>());
        }
    }

    public Map<AntColony, List<Hive>> getPlayers() {
        return players;
    }

    public List<Hive> getHiveListCopyOfPlayer(AntColony player) {
        synchronized (players) {
            return new ArrayList<>(players.getOrDefault(player, new ArrayList<>(0)));
        }
    }

    private Point getRandomPoint() {
        return new Point(randomGenerator.nextInt(0, maxWidth + 1), randomGenerator.nextInt(0, maxHeight + 1));
    }

    private FruitTree createNewFruitTree() {
        Point location = getRandomPoint();
        int foodProductionPeakPerDay = randomGenerator.nextInt(FRUIT_TREE_FOOD_PRODUCTION_PEAK_PER_DAY_MIN, FRUIT_TREE_FOOD_PRODUCTION_PEAK_PER_DAY_MAX);
        int foodProductionPeakDayOfYear = randomGenerator.nextInt(FRUIT_TREE_FOOD_PRODUCTION_PEAK_DAY_OF_YEAR_MIN, FRUIT_TREE_FOOD_PRODUCTION_PEAK_DAY_OF_YEAR_MAX);
        int dangerLevel = randomGenerator.nextInt(FRUIT_TREE_DANGER_LEVEL_MIN, FRUIT_TREE_DANGER_LEVEL_MAX + 1);
        int startingFood = randomGenerator.nextInt(FRUIT_TREE_FOOD_STARTING_MIN, FRUIT_TREE_FOOD_STARTING_MAX);

        return new FruitTree(this, location, startingFood, foodProductionPeakPerDay, foodProductionPeakDayOfYear, dangerLevel);
    }

    private Hive createNewHive(Point location, AntColony controllingPlayer, int startingWorker, int startingScouts, int startingWarrior, int startingFood) {
        return new Hive(this, location, controllingPlayer, startingWorker, startingScouts, startingWarrior, startingFood, COLONY_STARTING_ANT_CAPACITY, COLONY_STARTING_FOOD_CAPACITY);
    }

    private Hive createNewHive(Point location, AntColony controllingPlayer) {
        return createNewHive(location, controllingPlayer, COLONY_STARTING_WORKER, COLONY_STARTING_SCOUT, COLONY_STARTING_WARRIOR, COLONY_STARTING_FOOD);
    }

    public void generateNewWorld(long seed, int width, int height) {
        checkAndDistributePlayerColors();

        randomGenerator = new Random(seed);
        this.maxWidth = width;
        this.maxHeight = height;
        int numberFruitTrees = randomGenerator.nextInt(500, 800);
        synchronized (worldModel) {
            for (int idx = 0; idx < numberFruitTrees; idx++) {
                worldModel.addPointOfInterest(createNewFruitTree());
            }

            float degree = 360.0f / players.size();
            int playerCounter = 0;
            for (Map.Entry<AntColony, List<Hive>> entry : players.entrySet()) {
                Hive playerBase = createNewHive(AntColony.getPointOnCircle(new Point(width / 2, height / 2), Math.min(width / 3, height / 3), degree * playerCounter++),
                        entry.getKey());
                addNewHiveToGame(entry.getKey(), playerBase);
            }

            // TODO world generation
        }
    }

    private void checkAndDistributePlayerColors() {
        // This is a bit hacky, but automatically changes colors, so that they do not look too similar
        {
            // First, check if all players are compliant about taking the reassigned colors
            Color pickedColor;
            Color testColor;
            for (AntColony colony : players.keySet()) {
                pickedColor = colony.getColonyColor();
                testColor = new Color(0, 0, 0, 1.0);
                colony.assignNewColor(testColor);
                if (colony.getColonyColor() != testColor) {
                    throw new IllegalStateException("Player does not comply to color reassignment rule! Make sure, that the player is saving a reassigned color!");
                }
                colony.assignNewColor(pickedColor);
            }
        }
        final double colorDiff = 40.0 / 255.0;
        final double colorStep = 15.0 / 255.0;
        boolean iterateAgain = true;
        boolean colorTooSimilar;
        while (iterateAgain) {
            iterateAgain = false;
            for (AntColony colony : players.keySet()) {
                colorTooSimilar = true;
                while (colorTooSimilar) {
                    colorTooSimilar = false;
                    for (AntColony colonyCompare : players.keySet()) {
                        if (colony == colonyCompare)
                            continue;
                        if (Math.abs(colony.getColonyColor().getRed() - colonyCompare.getColonyColor().getRed()) < colorDiff &&
                                Math.abs(colony.getColonyColor().getGreen() - colonyCompare.getColonyColor().getGreen()) < colorDiff &&
                                Math.abs(colony.getColonyColor().getBlue() - colonyCompare.getColonyColor().getBlue()) < colorDiff) {
                            boolean smallerRed = colony.getColonyColor().getRed() < colonyCompare.getColonyColor().getRed();
                            colony.assignNewColor(shiftRedInColor(colony.getColonyColor(), (smallerRed ? -1 : 1) * colorStep));
                            colonyCompare.assignNewColor(shiftRedInColor(colonyCompare.getColonyColor(), (smallerRed ? 1 : -1) * colorStep));

                            boolean smallerGreen = colony.getColonyColor().getGreen() < colonyCompare.getColonyColor().getGreen();
                            colony.assignNewColor(shiftGreenInColor(colony.getColonyColor(), (smallerGreen ? -1 : 1) * colorStep));
                            colonyCompare.assignNewColor(shiftGreenInColor(colonyCompare.getColonyColor(), (smallerGreen ? 1 : -1) * colorStep));

                            boolean smallerBlue = colony.getColonyColor().getBlue() < colonyCompare.getColonyColor().getBlue();
                            colony.assignNewColor(shiftBlueInColor(colony.getColonyColor(), (smallerBlue ? -1 : 1) * colorStep));
                            colonyCompare.assignNewColor(shiftBlueInColor(colonyCompare.getColonyColor(), (smallerBlue ? 1 : -1) * colorStep));

                            colorTooSimilar = true;
                            iterateAgain = true;
                        }
                    }
                }
            }
        }
    }

    private Color shiftRedInColor(Color color, double byValue) {
        return new Color(Math.max(0, Math.min(1.0, color.getRed() + byValue)), color.getGreen(), color.getBlue(), 1.0);
    }

    private Color shiftGreenInColor(Color color, double byValue) {
        return new Color(color.getRed(), Math.max(0, Math.min(1.0, color.getGreen() + byValue)), color.getBlue(), 1.0);
    }

    private Color shiftBlueInColor(Color color, double byValue) {
        return new Color(color.getRed(), color.getGreen(), Math.max(0, Math.min(1.0, color.getBlue() + byValue)), 1.0);
    }

    /**
     * @return The winner of the game or null if there is no winner yet.
     */
    public AntColony getWinner() {
        if (players.isEmpty()) {
            System.out.println("The game ended with a draw. The last surviving players have been:");
            playersDiedThisDay.forEach(p -> System.out.println(p.getColonyName()));
            return playersDiedThisDay.get(playersDiedThisDay.size() - 1);
        } else if (players.size() == 1) {
            for (AntColony player : players.keySet()) {
                return player;
            }
        }
        return null;
    }

    public int getCurrentDay() {
        return currentDay;
    }

    public void simulateDay() {
        currentDay++;
        dayOfYear++;
        if (dayOfYear == 366) {
            dayOfYear = 1;
        }
        System.out.println("Simulate day " + currentDay + ". Year " + ((currentDay - 1) / 365 + 1) + ". Day of year " + dayOfYear + ".");

        checkForDeadPlayers();

        doHatching();

        tellPlayersNextDay();
        dailyPlayerReports();
        clearPlayerReports();

        doPlayerBreeding();

        doPlayerScouting();

        doPlayerHivePlanning();

        doPlayerAntDistribution();

        doAntGroupActions();

        simulatePOIs();
    }

    private void checkForDeadPlayers() {
        playersDiedThisDay.clear();
        Map<AntColony, List<Hive>> clonedMap = new HashMap<>(players); // because we remove from this list in the loop
        for (Map.Entry<AntColony, List<Hive>> entry : clonedMap.entrySet()) {
            List<Hive> clonedList = new ArrayList<>(entry.getValue()); // because we remove from this list in the loop
            for (Hive hive : clonedList) {
                if (hive.isEmpty()) {
                    destroyHive(hive);
                }
            }
            if (entry.getValue().isEmpty()) {
                playerLoses(entry.getKey());
            }
        }
    }

    private void addNewHiveToGame(AntColony controller, Hive newHive) {
        synchronized (players) {
            players.get(controller).add(newHive);
        }
        synchronized (worldModel) {
            worldModel.addPointOfInterest(newHive);
        }

        Map<PointOfInterest, AntGroup> casualties = new HashMap<>();
        Set<PointOfInterest> discoveries = new HashSet<>();
        Map<PointOfInterest, Float> collectedFood = new HashMap<>();
        Map<PointOfInterest, PoiIntel> sourceKnowledge = new HashMap<>();
        Set<PointOfInterest> destroyedPOIs = new HashSet<>();
        playerHiveReports.put(newHive, new PlayerHiveReport(casualties, discoveries, collectedFood, sourceKnowledge, destroyedPOIs));
    }

    private void destroyHive(Hive toDestroy) {
        toDestroy.destroy();
        toDestroy.getControllingPlayer(this).hiveDestroyed(toDestroy);
        synchronized (players) {
            players.get(toDestroy.getControllingPlayer(this)).remove(toDestroy);
        }
        playerHiveReports.remove(toDestroy);
        synchronized (worldModel) {
            worldModel.removePointOfInterest(toDestroy);
        }
        playerPlannedHives.remove(toDestroy);
    }

    private void playerLoses(AntColony player) {
        System.out.println("Player " + player.getColonyName() + " was defeated.");
        synchronized (players) {
            players.remove(player);
        }
        playersDiedThisDay.add(player);
    }

    private void doHatching() {
        playerBreedingAnts.forEach(Hive::addAntGroup);
        playerBreedingAnts.clear();
    }

    private void tellPlayersNextDay() {
        for (Map.Entry<AntColony, List<Hive>> entry : players.entrySet()) {
            entry.getKey().nextDay();
        }
    }

    private void dailyPlayerReports() {
        synchronized (players) {
            for (Map.Entry<AntColony, List<Hive>> entry : players.entrySet()) {
                for (Hive hive : entry.getValue()) {
                    entry.getKey().dailyReport(hive, playerHiveReports.get(hive));
                }
            }
        }
    }

    private void clearPlayerReports() {
        for (Map.Entry<Hive, PlayerHiveReport> entry : playerHiveReports.entrySet()) {
            entry.getValue().clearData();
        }
    }

    private void doPlayerBreeding() {
        float breedingCost;
        float breedingAntCount;
        synchronized (players) {
            for (Map.Entry<AntColony, List<Hive>> entry : players.entrySet()) {
                for (Hive hive : entry.getValue()) {
                    AntGroup plannedBreeding = entry.getKey().planBreeding(hive);
                    if (plannedBreeding == null) {
                        continue;
                    }
                    breedingAntCount = plannedBreeding.getWorkerCount() + plannedBreeding.getScoutCount() + plannedBreeding.getWarriorCount();
                    if (hive.getCurrentAntCount(entry.getKey()) + breedingAntCount > hive.getMaxAntCount(entry.getKey())) {
                        System.out.println("Player " + entry.getKey().getColonyName() + " tries to breed more Ants than there is space in the hive! Nothing is bred.");
                        continue;
                    }
                    breedingCost = plannedBreeding.getWorkerCount() * FOOD_COST_WORKER + plannedBreeding.getScoutCount() * FOOD_COST_SCOUT + plannedBreeding.getWarriorCount() * FOOD_COST_WARRIOR;
                    if (breedingCost > hive.getStoredFood(entry.getKey())) {
                        System.out.println("Player " + entry.getKey().getColonyName() + " tries to breed more Ants than there is available food in the hive! Nothing is bred.");
                        continue;
                    }

                    hive.useFood(breedingCost);
                    playerBreedingAnts.put(hive, plannedBreeding);
                }
            }
        }
    }

    private void doPlayerScouting() {
        synchronized (worldModel) {
            worldModel.clearScoutAreas();
            playerExplorationScoutCount.clear();
            synchronized (players) {
                for (Map.Entry<AntColony, List<Hive>> entry : players.entrySet()) {
                    for (Hive hive : entry.getValue()) {
                        ScoutArea scoutArea = entry.getKey().scoutArea(hive);
                        { // Check if scoutArea is valid
                            if (scoutArea == null) {
                                continue;
                            }
                            if (scoutArea.controllingHive() != hive) {
                                System.out.println("Player " + entry.getKey().getColonyName() + " tries to send Scouts from a different hive! Nothing is sent.");
                                continue;
                            }
                            if (hive.getCurrentScoutCount(entry.getKey()) < scoutArea.scoutCount()) {
                                System.out.println("Player " + entry.getKey().getColonyName() + " wants to scout with more ants than possible! Nothing is sent.");
                                continue;
                            }
                        }

                        // Do the scouting
                        playerExplorationScoutCount.put(hive, scoutArea.scoutCount());
                        for (PointOfInterest poi : worldModel.getPointsOfInterest()) {
                            if (scoutArea.isPointInArea(poi.getLocation())) {
                                if (randomGenerator.nextDouble() < scoutArea.scoutCount() * SCOUT_AREA_CHANCE_MODIFIER / scoutArea.getAreaSize()) {
                                    playerHiveReports.get(hive).discoveries().add(poi);
                                }
                            }
                        }
                        worldModel.addScoutArea(scoutArea);
                    }
                }
            }
        }
    }

    private void doPlayerHivePlanning() {
        // Count down all planned hive turns
        playerPlannedHives.replaceAll((hive, plannedHive) -> new PlannedHive(plannedHive.location, plannedHive.turns - 1));

        List<Map.Entry<Hive, PlannedHive>> plannedHives = playerPlannedHives.entrySet().stream().filter(plannedEntry -> plannedEntry.getValue().turns == 0).toList();
        // Make a list out of the stream to avoid concurrent access (removing in the loop)
        for (Map.Entry<Hive, PlannedHive> plannedEntry : plannedHives) {
            playerPlannedHives.remove(plannedEntry.getKey());
            Hive motherHive = plannedEntry.getKey();
            Hive newHive = createNewHive(plannedEntry.getValue().location, motherHive.getControllingPlayer(this), 0, 0, 0, 0);
            AntGroup colonyGroup = motherHive.getControllingPlayer(this).sendNewHiveGroup(motherHive, plannedEntry.getValue().location);
            if (colonyGroup.getWorkerCount() > motherHive.getCurrentWorkerCount(motherHive.getControllingPlayer(this))
                    || colonyGroup.getScoutCount() > motherHive.getCurrentScoutCount(motherHive.getControllingPlayer(this))
                    || colonyGroup.getWarriorCount() > motherHive.getCurrentWarriorCount(motherHive.getControllingPlayer(this))) {
                System.out.println("Player " + motherHive.getControllingPlayer(this).getColonyName() + " tried to send more ants than available to a new hive!");
            } else {
                motherHive.removeAntGroup(colonyGroup);
                if (colonyGroup.getAntCount() > COLONY_STARTING_ANT_CAPACITY) {
                    colonyGroup.losePercentOfGroup(1.0f - COLONY_STARTING_ANT_CAPACITY * 1.0f / colonyGroup.getAntCount());
                    System.out.println("Player " + motherHive.getControllingPlayer(this).getColonyName() + " tried to send more ants to the new hive than it has capacity. Excessive ants will die!");
                }
                newHive.addAntGroup(colonyGroup);
                addNewHiveToGame(motherHive.getControllingPlayer(this), newHive);
            }
        }

        synchronized (players) {
            for (Map.Entry<AntColony, List<Hive>> entry : players.entrySet()) {
                for (Hive hive : entry.getValue()) {
                    Point plannedPoint = entry.getKey().planNewHiveSendQueenTo(hive, this::checkIfPointIsValidForNewHive);
                    if (plannedPoint != null && checkIfPointIsValidForNewHive(plannedPoint)) {
                        if (hive.getStoredFood(hive.getControllingPlayer(this)) < FOOD_COST_QUEEN) {
                            System.out.println("Player " + hive.getControllingPlayer(this).getColonyName() + " tried to create a new hive, but had not enough food for a queen. Nothing happens!");
                        } else {
                            playerPlannedHives.put(hive, new PlannedHive(plannedPoint, HIVE_CREATION_DAYS));
                        }
                    }
                }
            }
        }
    }

    private boolean checkIfPointIsValidForNewHive(Point plannedHivePoint) {
        for (PlannedHive plannedHive : playerPlannedHives.values()) {
            if (plannedHive.getDistanceTo(plannedHivePoint) <= HIVE_MIN_DISTANCE_TO_HIVES)
                return false;
        }
        synchronized (worldModel) {
            return !(worldModel.isPointNearPOI(Hive.class, plannedHivePoint, HIVE_MIN_DISTANCE_TO_HIVES)
                    || worldModel.isPointNearPOI(FoodSource.class, plannedHivePoint, HIVE_MIN_DISTANCE_TO_FOOD_SOURCES));
        }
    }

    private void doPlayerAntDistribution() {
        synchronized (worldModel) {
            worldModel.getGlobalAntDistribution().clear();
            synchronized (players) {
                for (Map.Entry<AntColony, List<Hive>> entry : players.entrySet()) {
                    for (Hive hive : entry.getValue()) {
                        Map<PointOfInterest, AntGroup> distribution = entry.getKey().distributeAnts(hive);
                        if (distribution == null) {
                            distribution = new HashMap<>();
                        }

                        // Check if the controlling hive is set correctly
                        for (AntGroup group : distribution.values()) {
                            if (group.getControllingHive() != hive) {
                                System.out.println("Player " + entry.getKey().getColonyName() + " tries to distribute Ants from a different hive! Nothing is distributed.");
                                distribution.clear();
                                break;
                            }
                        }

                        // Check if the player is distributing more ants than possible
                        int workerCount = 0;
                        int scoutCount = playerExplorationScoutCount.getOrDefault(hive, 0);
                        int warriorCount = 0;
                        for (AntGroup group : distribution.values()) {
                            workerCount += group.getWorkerCount();
                            scoutCount += group.getScoutCount();
                            warriorCount += group.getWarriorCount();
                        }
                        if (hive.getCurrentWorkerCount(entry.getKey()) < workerCount
                                || hive.getCurrentScoutCount(entry.getKey()) < scoutCount
                                || hive.getCurrentWarriorCount(entry.getKey()) < warriorCount) {
                            System.out.println("Player " + entry.getKey().getColonyName() + " wants to distribute more ants than possible! Nothing is distributed.");
                        } else {
                            // Distribution is valid
                            distribution.forEach((poi, group) -> {
                                List<AntGroup> localGroups = worldModel.getGlobalAntDistribution().computeIfAbsent(poi, k -> new ArrayList<>());
                                localGroups.add(group);
                            });

                            // Add the remaining ants as AntGroup at the own hive (stationed ants)
                            List<AntGroup> localGroups = worldModel.getGlobalAntDistribution().computeIfAbsent(hive, k -> new ArrayList<>());
                            localGroups.add(new AntGroup(hive, hive.getCurrentWorkerCount(entry.getKey()) - workerCount,
                                    hive.getCurrentScoutCount(entry.getKey()) - scoutCount,
                                    hive.getCurrentWarriorCount(entry.getKey()) - warriorCount));
                        }
                    }
                }
            }
        }
    }

    private float getScoutingEstimation(float value, float error) {
        float offset = value * error;
        return randomGenerator.nextFloat(value - offset, value + offset + 1);
    }

    private void doAntGroupActions() {
        synchronized (worldModel) {
            worldModel.getGlobalAntDistribution().forEach((pointOfInterest, localAntGroups) -> {
                // Scouting
                {
                    for (AntGroup group : localAntGroups) {
                        if (playerHiveReports.containsKey(group.getControllingHive())) {
                            float scoutingError = (float) Math.pow(SCOUT_POI_ERROR_RATE, group.getScoutCount());
                            if (scoutingError < 0.0000001f) {
                                scoutingError = 0.0000001f;
                            }
                            // scoutingError is randomly used in range [1-error..1+error] for each value separately to not be able to deduce the actual error

                            float estimatedFood = 0;
                            float estimatedFoodProduction = 0;
                            float estimatedDangerLevel = 0;
                            boolean isOwnHive = false;
                            Set<AntGroup> estimatedEnemyGroups = new HashSet<>();
                            if (pointOfInterest instanceof FoodSource) {
                                estimatedFood = getScoutingEstimation(((FoodSource) pointOfInterest).getFoodAvailable(this), scoutingError);
                                estimatedFoodProduction = getScoutingEstimation(((FoodSource) pointOfInterest).getCurrentFoodProduction(dayOfYear), scoutingError);
                                estimatedDangerLevel = getScoutingEstimation(((FoodSource) pointOfInterest).getDangerLevel(), scoutingError);
                            } else if (pointOfInterest instanceof Hive) {
                                isOwnHive = pointOfInterest == group.getControllingHive();
                                if (!isOwnHive) {
                                    estimatedFood = getScoutingEstimation(((Hive) pointOfInterest).getStoredFood(((Hive) pointOfInterest).getControllingPlayer(this)), scoutingError);
                                    if (((Hive) pointOfInterest).isDestroyed()) {
                                        playerHiveReports.get(group.getControllingHive()).destroyedPOIs().add(pointOfInterest);
                                    }
                                }
                            }
                            for (AntGroup otherGroup : localAntGroups) {
                                if (group.getControllingHive() == otherGroup.getControllingHive())
                                    continue;
                                if (scoutingError < 0.05f || isOwnHive || scoutingError < randomGenerator.nextDouble()) {
                                    estimatedEnemyGroups.add(otherGroup.getPercentageOfGroup(randomGenerator.nextFloat(1 - scoutingError, 1 + scoutingError + 1)));
                                }
                            }

                            if (isOwnHive && estimatedEnemyGroups.isEmpty())
                                continue;
                            playerHiveReports.get(group.getControllingHive()).estimatedSourceKnowledge().put(pointOfInterest,
                                    new PoiIntel(estimatedFood, estimatedFoodProduction, estimatedDangerLevel, estimatedEnemyGroups));
                        } // else Hive of group does not exist anymore
                    }
                }

                // Combat
                {
                    Map<AntColony, Float> combatStrengthDistributionMap = new HashMap<>();
                    boolean checkForHiveDestruction = false;
                    float totalCombatStrength = 0;
                    float currentCombatStrength;
                    float addCombatStrength;
                    for (AntGroup group : localAntGroups) {
                        currentCombatStrength = combatStrengthDistributionMap.getOrDefault(group.getControllingHive().getControllingPlayer(this), 0.0f);
                        addCombatStrength = group.getCombatStrength(pointOfInterest);
                        totalCombatStrength += addCombatStrength;
                        combatStrengthDistributionMap.put(group.getControllingHive().getControllingPlayer(this), currentCombatStrength + addCombatStrength);
                    }
                    if (pointOfInterest instanceof Hive) {
                        // Defender of a hive gets a combat bonus
                        currentCombatStrength = combatStrengthDistributionMap.getOrDefault(((Hive) pointOfInterest).getControllingPlayer(this), 0.0f);
                        float newCombatStrength = (currentCombatStrength + COMBAT_STRENGTH_QUEEN) * (1 + COMBAT_HIVE_DEFENSE_BONUS);
                        totalCombatStrength += newCombatStrength - currentCombatStrength;
                        combatStrengthDistributionMap.put(((Hive) pointOfInterest).getControllingPlayer(this), newCombatStrength);
                        checkForHiveDestruction = true;
                    }
                    if (combatStrengthDistributionMap.size() > 1) {
                        // Do the combat calculation
                        for (AntGroup group : localAntGroups) {
                            float ratio = combatStrengthDistributionMap.get(group.getControllingHive().getControllingPlayer(this)) / totalCombatStrength;
                            if (ratio < COMBAT_STRENGTH_RATIO_DECISIVE_VICTORY_THRESHHOLD) {
                                // Losses (%) are calculated with: e^(-ratio*MODIFIER) / (#players-1)
                                processAntGroupCasualties(group.getControllingHive(), pointOfInterest,
                                        group.losePercentOfGroup((float) (Math.exp(-ratio * COMBAT_STRENGTH_RATIO_MODIFIER) / (combatStrengthDistributionMap.size() - 1))));
                                if (checkForHiveDestruction && group.getControllingHive().getControllingPlayer(this) == ((Hive) pointOfInterest).getControllingPlayer(this)
                                        && ratio < COMBAT_STRENGTH_RATIO_HIVE_DESTRUCTION_THRESHHOLD && !((Hive) pointOfInterest).isDestroyed()) {
                                    destroyHive((Hive) pointOfInterest);
                                }
                            } // else decisive victory => no losses
                        }
                    }
                    if (checkForHiveDestruction && ((Hive) pointOfInterest).isDestroyed()) {
                        for (AntGroup group : localAntGroups) {
                            if (group.getControllingHive() == pointOfInterest)
                                continue;
                            if (group.getControllingHive().isDestroyed())
                                continue;
                            playerHiveReports.get(group.getControllingHive()).destroyedPOIs().add(pointOfInterest);
                        }
                    }
                }

                // Natural danger
                {
                    if (pointOfInterest instanceof FoodSource) {
                        localAntGroups.forEach(group -> {
                            // Losses (%) are calculated with: 1 - (1 - DANGER_RATIO) ^ dangerLevel
                            processAntGroupCasualties(group.getControllingHive(), pointOfInterest,
                                    group.losePercentOfGroup((float) (1.0f - Math.pow(1.0f - DANGER_LEVEL_DEATH_RATIO, ((FoodSource) pointOfInterest).getDangerLevel()))));
                        });
                    } // else no danger
                }

                // Collect Food
                {
                    float availableFood = 0;
                    if (pointOfInterest instanceof FoodSource) {
                        availableFood = ((FoodSource) pointOfInterest).getFoodAvailable(this);
                    } else if (pointOfInterest instanceof Hive) {
                        availableFood = ((Hive) pointOfInterest).getStoredFood(((Hive) pointOfInterest).getControllingPlayer(this));
                    }

                    Map<Hive, Float> hiveGatherStrength = new HashMap<>();
                    float totalGatherSum = 0.0f;
                    float currentGatherValue;
                    float addGatherValue;
                    for (AntGroup group : localAntGroups) {
                        if (pointOfInterest == group.getControllingHive()) {
                            continue; // Can't gather from your controlling hive (but from other hives in your control)
                        }
                        currentGatherValue = hiveGatherStrength.getOrDefault(group.getControllingHive(), 0.0f);
                        addGatherValue = (float) (group.getWorkerCount() * FOOD_GATHER_PER_WORKER
                                * Math.pow(1 - FOOD_GATHER_TRAVEL_MALUS, group.getControllingHive().getDistanceTo(pointOfInterest.getLocation()) / 100.0f));
                        totalGatherSum += addGatherValue;
                        hiveGatherStrength.put(group.getControllingHive(), currentGatherValue + addGatherValue);
                    }
                    if (totalGatherSum > availableFood) {
                        // There is not enough food for all workers.
                        float ratio = availableFood / totalGatherSum;
                        hiveGatherStrength.replaceAll((hive, gather) -> gather * ratio);
                        totalGatherSum = availableFood;
                    }
                    hiveGatherStrength.forEach((hive, gather) -> {
                        if (!hive.isDestroyed()) {
                            hive.collectFood(gather);
                            playerHiveReports.get(hive).collectedFood().put(pointOfInterest, gather);
                        }
                    });

                    if (pointOfInterest instanceof FoodSource) {
                        ((FoodSource) pointOfInterest).foodGathered(totalGatherSum);
                    } else if (pointOfInterest instanceof Hive) {
                        ((Hive) pointOfInterest).useFood(totalGatherSum);
                    }
                }

                // Build up own hive
                {
                    if (pointOfInterest instanceof Hive) {
                        for (AntGroup group : localAntGroups) {
                            if (!group.getControllingHive().isEnemyOf(((Hive) pointOfInterest).getControllingPlayer(this))) {
                                ((Hive) pointOfInterest).buildBiggerHive(group.getWorkerCount());
                            }
                        }
                    }
                }
            });
        }
    }

    private void processAntGroupCasualties(Hive controllingHive, PointOfInterest pointOfInterest, AntGroup casualties) {
        if (playerHiveReports.containsKey(controllingHive)) {
            playerHiveReports.get(controllingHive).antCasualties().merge(pointOfInterest, casualties, AntGroup::merge);
        }
        controllingHive.removeAntGroup(casualties);
    }

    private void simulatePOIs() {
        synchronized (worldModel) {
            worldModel.getPointsOfInterest().forEach(pointOfInterest -> pointOfInterest.simulateDay(dayOfYear));
        }
    }
}
