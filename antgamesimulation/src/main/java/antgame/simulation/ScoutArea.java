package antgame.simulation;

import antgame.players.AntColony;
import antgame.data.Point;

public record ScoutArea(Hive controllingHive, Point center, int radius, int scoutCount) {
    public AntColony getControllingPlayer(Simulation authority) {
        return controllingHive.getControllingPlayer(authority);
    }

    public Point getCenter(Simulation authority) {
        if (controllingHive.authority == authority) {
            return center;
        }
        return null;
    }

    public int getRadius(Simulation authority) {
        if (controllingHive.authority == authority) {
            return radius;
        }
        return -1;
    }

    private double getDistanceTo(Point point) {
        return Math.sqrt(Math.pow(point.x - center.x, 2) + Math.pow(point.y - center.y, 2));
    }

    public boolean isPointInArea(Point point) {
        return getDistanceTo(point) < radius;
    }

    public double getAreaSize() {
        return Math.PI * radius * radius;
    }
}
