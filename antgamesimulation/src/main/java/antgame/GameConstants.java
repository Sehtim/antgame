package antgame;

public class GameConstants {
    public static final int HIVE_MIN_DISTANCE_TO_HIVES = 100;
    public static final int HIVE_MIN_DISTANCE_TO_FOOD_SOURCES = 20;
    public static final int HIVE_CREATION_DAYS = 5;
    public static final float HIVE_NATURAL_DEATH_RATE = 0.01f;
    public static final int HIVE_GROW_ANT_CAPACITY = 5;
    public static final int HIVE_GROW_FOOD_CAPACITY = 50;
    public static final float HIVE_GROW_PER_WORKER_FACTOR = 0.01f;

    public static final int COLONY_STARTING_ANT_CAPACITY = 500;
    public static final int COLONY_STARTING_FOOD_CAPACITY = 10000;
    public static final int COLONY_STARTING_WORKER = 200;
    public static final int COLONY_STARTING_SCOUT = 100;
    public static final int COLONY_STARTING_WARRIOR = 25;
    public static final int COLONY_STARTING_FOOD = 10000;

    public static final float SCOUT_AREA_CHANCE_MODIFIER = 10.0f; // Higher values make scouting areas easier
    public static final float SCOUT_POI_ERROR_RATE = 0.991f; // old 0.993f

    public static final float FOOD_HUNGER_DEATH_RATE = 0.4f;
    public static final float FOOD_DECAY_RATE = 0.02f;
    public static final float FOOD_GATHER_TRAVEL_MALUS = 0.25f; // old 0.2f
    public static final float FOOD_GATHER_PER_WORKER = 8.0f; // old 7.0f

    public static final float FOOD_COST_WORKER = 2.0f;
    public static final float FOOD_COST_SCOUT = 1.0f;
    public static final float FOOD_COST_WARRIOR = 5.0f;
    public static final float FOOD_COST_QUEEN = 1000.0f;

    public static final float FOOD_DRAIN_WORKER = 1.0f;
    public static final float FOOD_DRAIN_SCOUT = 0.5f; // old 1.0f
    public static final float FOOD_DRAIN_WARRIOR = 2.0f;
    public static final float FOOD_DRAIN_QUEEN = 100.0f;

    public static final float COMBAT_STRENGTH_WORKER = 2.0f;
    public static final float COMBAT_STRENGTH_SCOUT = 1.0f;
    public static final float COMBAT_STRENGTH_WARRIOR = 15.0f;
    public static final float COMBAT_STRENGTH_QUEEN = 70.0f;
    public static final float COMBAT_TRAVEL_EXHAUSTION_MALUS = 0.05f;
    public static final float COMBAT_HIVE_DEFENSE_BONUS = 0.20f;
    public static final float COMBAT_STRENGTH_RATIO_MODIFIER = 4.0f; // Higher values result in less casualties on all sides
    public static final float COMBAT_STRENGTH_RATIO_DECISIVE_VICTORY_THRESHHOLD = 0.95f;
    public static final float COMBAT_STRENGTH_RATIO_HIVE_DESTRUCTION_THRESHHOLD = 0.10f;

    public static final float DANGER_LEVEL_DEATH_RATIO = 0.05f; // old 0.02f

    public static final int FRUIT_TREE_FOOD_STARTING_MIN = 200; // old 0
    public static final int FRUIT_TREE_FOOD_STARTING_MAX = 1300; // old 500
    public static final int FRUIT_TREE_FOOD_PRODUCTION_PEAK_DAY_OF_YEAR_MIN = 50;
    public static final int FRUIT_TREE_FOOD_PRODUCTION_PEAK_DAY_OF_YEAR_MAX = 250;
    public static final int FRUIT_TREE_FOOD_PRODUCTION_PEAK_PER_DAY_MIN = 250;
    public static final int FRUIT_TREE_FOOD_PRODUCTION_PEAK_PER_DAY_MAX = 750;
    public static final int FRUIT_TREE_FOOD_PRODUCTION_STD_DEVIATION = 50;
    public static final int FRUIT_TREE_DANGER_LEVEL_MIN = 0;
    public static final int FRUIT_TREE_DANGER_LEVEL_MAX = 20; // old 3
}
