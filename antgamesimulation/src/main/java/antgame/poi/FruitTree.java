package antgame.poi;

import antgame.GameConstants;
import antgame.simulation.FoodSource;
import antgame.simulation.Simulation;
import antgame.data.Point;


public class FruitTree extends FoodSource {
    public FruitTree(Simulation authority, Point location, int foodAvailable, int foodProductionPeakPerDay, int foodProductionPeakDayOfYear, int dangerLevel) {
        super(authority, location, foodAvailable, foodProductionPeakPerDay, foodProductionPeakDayOfYear, dangerLevel, GameConstants.FRUIT_TREE_FOOD_PRODUCTION_STD_DEVIATION);
    }
}
