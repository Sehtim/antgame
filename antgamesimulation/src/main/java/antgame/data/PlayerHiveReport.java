package antgame.data;

import antgame.simulation.AntGroup;
import antgame.simulation.PointOfInterest;
import antgame.GameConstants;
import antgame.simulation.ScoutArea;

import java.util.Map;
import java.util.Set;

/**
 * This class is a composition of divers information collected by a hive.
 *
 * @param antCasualties            A map containing how many ants died from combat plus natural danger per {@link PointOfInterest}.
 *                                 Natural deaths are not in the statistic.
 *                                 <br>
 *                                 (see {@link GameConstants#COMBAT_STRENGTH_RATIO_MODIFIER}) <br>
 *                                 (see {@link GameConstants#DANGER_LEVEL_DEATH_RATIO}) <br>
 *                                 (see {@link GameConstants#HIVE_NATURAL_DEATH_RATE}) <br>
 * @param discoveries              A set of {@link PointOfInterest}s which have been spotted within the {@link ScoutArea} of the previous day.
 *                                 The simulation does not store, which POIs have already been discovered.
 *                                 Therefor the set can contain POIs which have been discovered some day earlier.
 *                                 <br>
 * @param collectedFood            A map containing the amount of food collected per {@link PointOfInterest}.
 *                                 <br>
 * @param estimatedSourceKnowledge A map containing {@link PoiIntel} collected by the scouts per {@link PointOfInterest}.
 *                                 <br>
 *                                 (see {@link GameConstants#SCOUT_POI_ERROR_RATE}) <br>
 * @param destroyedPOIs            A set containing all destroyed {@link PointOfInterest}s,
 *                                 where ants of the hive have been present the last day and the POI got destroyed on the last day or any day before.
 *                                 <br>
 */
public record PlayerHiveReport(Map<PointOfInterest, AntGroup> antCasualties,
                               Set<PointOfInterest> discoveries,
                               Map<PointOfInterest, Float> collectedFood,
                               Map<PointOfInterest, PoiIntel> estimatedSourceKnowledge,
                               Set<PointOfInterest> destroyedPOIs) {

    public void clearData() {
        antCasualties.clear();
        discoveries.clear();
        collectedFood.clear();
        estimatedSourceKnowledge.clear();
        destroyedPOIs.clear();
    }
}
