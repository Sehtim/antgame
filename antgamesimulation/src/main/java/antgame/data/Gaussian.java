package antgame.data;

public class Gaussian {
    private final double variance;

    public Gaussian(double stdDeviation) {
        this.variance = stdDeviation * stdDeviation;
    }

    public double getY(double x) {
        return Math.exp(-(x * x) / (2 * variance));
    }
}
