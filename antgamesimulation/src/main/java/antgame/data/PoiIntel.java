package antgame.data;

import antgame.simulation.AntGroup;

import java.util.Set;

public record PoiIntel(float estimatedFood, float estimatedFoodProduction, float estimatedDangerLevel,
                       Set<AntGroup> estimatedEnemyAntgroups) {
}
