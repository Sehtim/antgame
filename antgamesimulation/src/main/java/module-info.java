module antgamesimulation {
    exports antgame.data;
    exports antgame.simulation;
    exports antgame;
    exports antgame.players;
    exports antgame.poi;
    requires java.desktop;
}